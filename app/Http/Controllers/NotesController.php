<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notes;

class NotesController extends Controller
{
    public function index()
    {
        return Notes::all();
    }
 
    public function show($id)
    {
        return Notes::find($id);
    }

    public function store(Request $request)
    {
        $note = new Notes;

        $note->text = $request->input('text');
        $note->task_id = $request->input('taskId');
        $note->save();
    }

    public function update(Request $request, $id)
    {
        $notes = Notes::find($id);
        $notes->text = $request->input('text');
        $notes->save();

        return $notes;
        

    }

    public function delete(Request $request, $id)
    {
        $notes = Notes::findOrFail($id);
        $notes->delete();

        return 204;
    }
}
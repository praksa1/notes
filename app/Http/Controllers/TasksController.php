<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tasks;

class TasksController extends Controller
{
    public function index()
    {
        return Tasks::all();
    }
 
    public function show($id)
    {
        return Tasks::findOrFail($id);
    }

    public function store(Request $request)
    {
        $tasks = new Tasks;

        $tasks->title = $request->input('title');
        $tasks->desription = $request->input('description');
        $tasks->user_id = $request->input('userId');
        
        
        $tasks->save();
        return $tasks;
    }

    public function update(Request $request, $id)
    {
        $tasks = Tasks::findOrFail($id);
        $tasks->title = $request->input('title');
        $tasks->desription = $request->input('desription');
        $tasks->save();

        return $tasks;
    }

    public function destroy(Request $request, $id)
    {
        $tasks = Tasks::findOrFail($id);
        $tasks->delete();

        return 204;
    }
}
<?php

use Illuminate\Http\Request;
use app\Notes;
use app\Tasks;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('login', 'PassportController@login');
Route::post('register', 'PassportController@register');
 
Route::get('user', 'PassportController@details');

Route::resource('notes', 'NotesController');

Route::resource('tasks', 'TasksController');